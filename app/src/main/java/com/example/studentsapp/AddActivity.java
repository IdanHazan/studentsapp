package com.example.studentsapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.studentsapp.model.Model;
import com.example.studentsapp.model.Student;

public class AddActivity extends AppCompatActivity  {
    EditText nameEt;
    EditText idEt;
    EditText phoneEt;
    EditText addressEt;
    CheckBox cb;
    TextView messageTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        nameEt = findViewById(R.id.add_name_et);

        idEt = findViewById(R.id.add_id_et);

        cb = findViewById(R.id.add_cb);

        phoneEt = findViewById(R.id.add_phone_ep);

        addressEt = findViewById(R.id.add_address_ep);

        messageTv = findViewById(R.id.add_error_tv);

        Button saveBtn = findViewById(R.id.add_save_btn);
        Button cancelBtn = findViewById(R.id.add_cancel_btn);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add();
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backToList();
            }
        });

    }

    private void add() {
        String name = nameEt.getText().toString();
        String id = idEt.getText().toString();
        String phone = phoneEt.getText().toString();
        String address = addressEt.getText().toString();
        boolean flag = cb.isChecked();
        String message = Model.instance.addStudent(new Student(name, id, address, phone, flag));

        if(message != null){
            messageTv.setText(message);
        } else {
            backToList();
        }
        Log.d("TAG","saved name:" + name + " id:" + id + " flag:" + flag + " adress: " + address + " phone: " + phone);
    }

    private void backToList() {
        Intent i = new
                Intent(getApplicationContext(),
                StudentListRvAcivity.class);
        startActivity(i);
    }
}