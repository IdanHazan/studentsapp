package com.example.studentsapp.model;


import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

public class Model {
    public static final Model instance = new Model();

    private Model(){
        for(int i=0;i<10;i++){
            Student s = new Student("name " + i ,""+i, "address " + i, "0525070707",false);
            data.add(s);
        }
    }

    List<Student> data = new LinkedList<Student>();

    public List<Student> getAllStudents(){
        return data;
    }

    public String setStudent(Student lastStd, Student newStd) {

        if(newStd.getId().equals("")) {
            return "Id cannot be empty";
        }

        // Check if student already exists
        int pos = findPositionById(lastStd.getId());

        // Check the new student id is not exists
        int newPos = findPositionById(newStd.getId());

        if(pos != -1 && ( newPos == -1 || lastStd.getId().equals(newStd.getId()))) {
            Student std = data.get(pos);
            std.setName(newStd.getName());
            std.setId(newStd.getId());
            std.setFlag(newStd.isFlag());
            std.setAddress(newStd.getAddress());
            std.setPhone(newStd.getPhone());

        } else {
            return "Id already exists";
        }
        return null;
    }

    public String addStudent(Student student){

        if(student.getId().equals("")) {
            return "Id cannot be empty";
        }

        int pos = findPositionById(student.getId());

        if(pos == -1) {
            data.add(student);
        } else {
            return "Id already exists";
        }
        return null;
    }

    public void deleteStudent(Student student){
        int pos = findPositionById(student.getId());

        if(pos != -1) {
            data.remove(pos);
        }
    }

    public int findPositionById(String studentId) {
        for (int i=0;i<data.size();i++) {
            Student std = data.get(i);
            if (std.getId().equals(studentId)) {
                return i;
            }
        }
        return -1;
    }
}

