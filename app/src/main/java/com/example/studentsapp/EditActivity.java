package com.example.studentsapp;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.example.studentsapp.model.Model;
import com.example.studentsapp.model.Student;

public class EditActivity extends AppCompatActivity  {
    EditText nameEt;
    EditText idEt;
    EditText phoneEt;
    EditText addressEt;
    CheckBox cb;
    Student lastStd;
    TextView messageTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        Student s = (Student) getIntent().getSerializableExtra("selectedStd");
        lastStd = s;


        nameEt = findViewById(R.id.edit_name_et);
        nameEt.setText(s.getName());

        idEt = findViewById(R.id.edit_id_et);
        idEt.setText(s.getId());

        cb = findViewById(R.id.edit_cb);
        cb.setChecked(s.isFlag());

        phoneEt = findViewById(R.id.edit_phone_ep);
        phoneEt.setText(s.getPhone());

        addressEt = findViewById(R.id.edit_address_ep);
        addressEt.setText(s.getAddress());

        messageTv = findViewById(R.id.edit_error_tv);

        Button saveBtn = findViewById(R.id.edit_save_btn);
        Button cancelBtn = findViewById(R.id.edit_cancel_btn);
        Button deleteBtn = findViewById(R.id.edit_delete_btn);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save(lastStd);
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backToList();
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                delete(lastStd);
            }
        });
    }

    private void save(Student lastStd) {
        String name = nameEt.getText().toString();
        String id = idEt.getText().toString();
        String phone = phoneEt.getText().toString();
        String address = addressEt.getText().toString();
        boolean flag = cb.isChecked();
        String message = Model.instance.setStudent(lastStd, new Student(name, id, address, phone, flag));

        if(message != null) {
            messageTv.setText(message);
        } else {
            backToList();
        }

        Log.d("TAG","saved name:" + name + " id:" + id + " flag:" + flag);
    }

    private void delete(Student lastStd) {
        Model.instance.deleteStudent(lastStd);
        backToList();
    }


    private void backToList() {
        Intent i = new
                Intent(getApplicationContext(),
                StudentListRvAcivity.class);
        startActivity(i);
    }
}