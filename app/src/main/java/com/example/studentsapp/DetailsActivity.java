package com.example.studentsapp;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.studentsapp.model.Student;

public class DetailsActivity extends AppCompatActivity {
    EditText nameEt;
    EditText idEt;
    EditText phoneEt;
    EditText addressEt;
    CheckBox cb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        Student s = (Student) getIntent().getSerializableExtra("selectedStd");

        nameEt = findViewById(R.id.details_name_et);
        nameEt.setKeyListener(null);
        nameEt.setText(s.getName());

        idEt = findViewById(R.id.details_id_et);
        idEt.setKeyListener(null);
        idEt.setText(s.getId());


        cb = findViewById(R.id.details_cb);
        cb.setKeyListener(null);
        cb.setChecked(s.isFlag());

        phoneEt = findViewById(R.id.details_phone_ep);
        phoneEt.setKeyListener(null);
        phoneEt.setText(s.getPhone());

        addressEt = findViewById(R.id.details_address_ep);
        addressEt.setKeyListener(null);
        addressEt.setText(s.getAddress());

        Button editBtn = findViewById(R.id.add_cancel_btn);

        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new
                        Intent(getApplicationContext(),
                        EditActivity.class);
                i.putExtra("selectedStd", s);
                startActivity(i);
            }
        });
    }

}